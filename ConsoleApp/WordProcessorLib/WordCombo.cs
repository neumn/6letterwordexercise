﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WordProcessorLib {

  /// <summary>
  /// A word and all his possible combinations found
  /// </summary>
  public class WordCombo {

    public WordCombo(string word, bool onlyUniqueCombos) {
      Word = word;
      OnlyUniqueCombos = onlyUniqueCombos;

      Combos = new List<Combo>();
    }

    /// <summary>
    /// The word of 6 (or x) character for which we search combo's.
    /// </summary>
    public string Word { get; private set; }

    /// <summary>
    /// It's an option to enable/disable the same kind of combinations.
    /// like f + oobar, f + oobar
    /// </summary>
    public bool OnlyUniqueCombos { get; private set; }

    /// <summary>
    /// a list of valid combo's
    /// </summary>
    public List<Combo> Combos { get; private set; }

    /// <summary>
    /// This methods assures that a combo is valid before adding it to the list
    /// </summary>
    /// <param name="combo"></param>
    public void AddCombo(Combo combo) {

      if (combo.ToString() != Word) {
        throw new ArgumentException(combo.ToString() + " doesn't match " + Word);
      }

      // unique treatment
      if (OnlyUniqueCombos && Combos.Any(x => x.ToFormattedString() == combo.ToFormattedString())) {
        return;
      }

      Combos.Add(combo);
    }

    public override string ToString() {
      return $"Word -> {Word} ({Combos.Count()} combos found)\n" +
      Combos.Select(x => x.ToFormattedString()).Aggregate((s, t) => s + "\n" + t);
    }
  }
}
