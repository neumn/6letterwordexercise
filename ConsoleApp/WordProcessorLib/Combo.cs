﻿using System.Collections.Generic;
using System.Linq;

namespace WordProcessorLib {

  /// <summary>
  /// A Combo is a list of wordparts that could make up a word of 6 (or x) characters, when concatenated
  /// It's a simple construction block, no validation required here.
  /// </summary>
  public class Combo {

    private string _value = ""; // constructed word so far, avoiding reconstruction from wordParts in recursive loops

    public List<string> WordParts { get; private set; } = new List<string>();

    public Combo() { }

    public void AddWordPart(string wordPart) {
      WordParts.Add(wordPart);
      _value += wordPart;
    }

    public override string ToString() {
      return _value;
    }

    public string ToFormattedString() {
      if (WordParts == null || WordParts.Any() == false) {
        return "";
      }

      return WordParts.Aggregate((a, b) => a + "+" + b);
    }

    /// <summary>
    /// In recusive loops, we need the state of the higher level combo to be retained.
    /// Therefor we need to be able to create a copy.
    /// </summary>
    /// <param name="combo"></param>
    /// <returns></returns>
    public static Combo CreateCopy(Combo combo) {
      var comboCopy = new Combo();

      foreach (var wordpart in combo.WordParts) {
        comboCopy.AddWordPart(wordpart);
      }

      return comboCopy;
    }
  }
}
