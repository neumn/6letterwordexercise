﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace WordProcessorLib {

  /// <summary>
  /// This class influences behavior of the WordProcessor by providing options.
  /// This is the 
  /// </summary>
  public class WordProcessorOptions {

    /// <summary>
    /// Length of the words where trying to assemble
    /// </summary>
    public int WordLength { get; set; }

    /// <summary>
    /// Only hold unique combinations
    /// </summary>
    public bool OnlyUniqueCombos { get; set; }
  }

  /// <summary>
  /// This class distributes the work over different tasks.
  /// The work is to find combinations that make a whole word of 6 (or x) characters.
  /// </summary>
  public class WordProcessor {

    public WordProcessor(WordProcessorOptions wordProcessorOptions, IEnumerable<string> words) {
      WordLengthToConstruct = wordProcessorOptions.WordLength;
      OnlyUniqueCombos = wordProcessorOptions.OnlyUniqueCombos;

      xCharacterWordBag = new Dictionary<string, WordCombo>(
        words
        .Where(x => x.Length == WordLengthToConstruct)
        .Distinct()
        .ToDictionary(x => x, x => new WordCombo(x, OnlyUniqueCombos))
        );

      WordParts = words.Where(x => x.Length < WordLengthToConstruct).ToList();
    }

    public int WordLengthToConstruct { get; private set; }

    public bool OnlyUniqueCombos { get; private set; }

    /// <summary>
    /// The key contains the words we're trying to assemble of 6 (or x) characters
    /// The value contains an object with all valid combo's for that word
    /// </summary>
    public Dictionary<string, WordCombo> xCharacterWordBag { get; set; }

    /// <summary>
    /// WordParts are all words smaller than 6 (or x) characters
    /// With these parts we will try to construct the words of 6 (or x) characters
    /// </summary>
    public List<string> WordParts { get; set; }

    /// <summary>
    /// start distributing the tasks for constructing all x-letter words.
    /// </summary>
    /// <param name="ct">Cancelation token</param>
    public void StartProcess(CancellationToken ct) {
      Console.WriteLine($"{xCharacterWordBag.Keys.Count} words to construct with length {WordLengthToConstruct}.");
      Console.WriteLine($"{WordParts.Count()} wordparts found to analyse.");
      Console.WriteLine($"Starting Process. Press 'c' to cancel.");

      var stopwatch = new Stopwatch();
      stopwatch.Start();

      var parallelOptions = new ParallelOptions { CancellationToken = ct };

      Parallel.ForEach(xCharacterWordBag.Keys, parallelOptions, x => { FindWordCombos(x, WordParts.Where(p => x.Contains(p)).ToList(), null, parallelOptions.CancellationToken); });

      stopwatch.Stop();

      foreach (var item in xCharacterWordBag) {
        ct.ThrowIfCancellationRequested();
        Console.WriteLine($"{item.Value}");
      }

      Console.WriteLine($"Time spent: {stopwatch.Elapsed}");
    }

    /// <summary>
    /// Recursive algorithm to create combinations of wordParts matching the word
    /// Each level we go deeper we leave 1 additional wordpart out from the collection of wordparts, so that previous wordparts won't be reused in 1 wordconstruction
    /// </summary>
    /// <param name="wordToConstruct"></param>
    /// <param name="wordParts"></param>
    /// <param name="previousLevelCombo">Previous level combo state must be retained as we can have multiple new combos on the same level</param>
    public void FindWordCombos(string wordToConstruct, List<string> wordParts, Combo previousLevelCombo, CancellationToken ct) {
      if (wordParts.Any() == false) // if there aren't any wordparts left, nothing to do.
        return;

      foreach (var wordPart in wordParts) {
        ct.ThrowIfCancellationRequested();

        var wordSoFar = (previousLevelCombo?.ToString() ?? "") + wordPart; // previous level is null on level 0

        if (wordSoFar.Length > wordToConstruct.Length) {
          continue; // invalid wordPart
        }

        if (wordToConstruct.StartsWith(wordSoFar)) {

          var combo = previousLevelCombo == null ? new Combo()
                                                 : Combo.CreateCopy(previousLevelCombo); // previous level state must be retained during loop

          combo.AddWordPart(wordPart);

          if (combo.ToString() == wordToConstruct) {
            xCharacterWordBag[wordToConstruct].AddCombo(combo);
            break; // end of tree, but there can be more combo's on the same level;
          }

          FindWordCombos(wordToConstruct, wordParts.Except(combo.WordParts).ToList(), combo, ct); // new branch
        }
      }
    }
  }
}
