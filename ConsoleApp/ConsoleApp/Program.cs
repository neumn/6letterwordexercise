﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using WordProcessorLib;

namespace ConsoleApp2 {

  /*
  The WordProcessorLib fullfils the exercise found at 
  https://bitbucket.org/behindthebuttons/6letterwordexercise/src/master/
  It's most important class, the WordProcessor,
  accepts a list of words on which it will do the analysis.
  As such it is independent of the way the source of words is retrieved.
  This makes it reusable if you want to use other kinds of (word)-sources.

  The extensibility point is implemented by the WordProcessorOptions.
  Which contains currently 2 options to influence the behavior of the WordProcessor.
  (namely, 6 character-length-words and disabling non-unique combinations,
  like f+oobar, f+oobar)

  The Program-class does very-little.
  It reads the words from a file.
  and calls the WordProcessor to do its job.

  The options and the source retrieval is currently fixed for simplicity, 
  as the main focus of this exercise,
  went to the performance and the algorithm of the WordProcessor.
  */

  public class Program {

    public static async Task Main(string[] args) {
      var cts = new CancellationTokenSource();

      Console.WriteLine("Hello from the 6 Letter Word Program!");

      var wordList = await GetWordsAsync();

      var options = new WordProcessorOptions {
        WordLength = 6,
        OnlyUniqueCombos = true
      };
      var wordProcessor = new WordProcessor(options, wordList);

      ListenForCancelation(cts);

      try {
        wordProcessor.StartProcess(cts.Token);
      }
      catch (OperationCanceledException ex) {
        Console.WriteLine("Stop processing. Cancelation was requested.");
      }
      finally {
        cts.Dispose();
      }

      Console.WriteLine("Press Enter to quit.");
      Console.ReadLine();
    }

    /// <summary>
    /// Get the words from the file.
    /// </summary>
    /// <returns></returns>
    public static async Task<string[]> GetWordsAsync() {
      Console.WriteLine("Reading file from " + Directory.GetCurrentDirectory().ToString() + "\\input.txt");

      if (File.Exists(Directory.GetCurrentDirectory().ToString() + "\\input.txt") == false) {
        Console.WriteLine("File not found.");
      }

      using (var reader = File.OpenText("input.txt")) {
        var fileText = await reader.ReadToEndAsync();

        var words = fileText.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);

        Console.WriteLine($"{words.Length} words read.");

        return words;
      }
    }

    public static void ListenForCancelation(CancellationTokenSource cts) {
      Task.Run(() =>
      {
        if (Console.ReadKey().KeyChar == 'c') {
          cts.Cancel();
          Console.WriteLine();
        }
      });
    }
  }
}
